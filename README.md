# GFL Assembler

This repo contains the infrastructure with all required containers and instructions.
Its main task is to have everything together in declarative way and to install the
infrastructure when required.

*Some* values, e.g. like ingress domain (based on corresponding DNS entry), are 
specific to the target environment. They are subject to replacement by argo-cd.

## Services

In general the `develop` branch here will list service images taged as `develop`.
So the `main` branch will point to images built from `main`.

### Solr

You know, for search.

Project: https://gitlab.gwdg.de/farbenlehre/gfl-indexer

### Web

Symfony app with TextAPI bundle and the complete website.

Project: https://gitlab.gwdg.de/farbenlehre/gfl-web

### S3API

Providing access to the images stored at S3.

Project: https://gitlab.gwdg.de/farbenlehre/s3api

## Secrets

All secrets not be injected from [Vault](https://secs.sub.uni-goettingen.de)
by argo-cd.

## Local Development

A working compond of the service will require current images referenced in the
[docker-compose.yaml](docker-compose.yaml) and also the required env vars are 
to be set.

The solr index has some hardcoded domain name, that will prevent Tido from 
working correctly. Build with `MAIN_DOMAIN=http://localhost` to make it
work.

```bash
docker compose up -d
```

Initially it should complain on some missing env files. So please add

- .env-indexer
- .env-s3
- .env-web
- .env-nginx

with all the appropriate content. See the respective git repos for details.
